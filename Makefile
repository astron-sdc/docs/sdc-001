DOCNAME=SDC-001
export TEXMFHOME ?= astron-texmf/texmf

$(DOCNAME).pdf: $(DOCNAME).tex meta.tex
	xelatex $(DOCNAME)
	makeglossaries $(DOCNAME)
	biber $(DOCNAME)
	xelatex $(DOCNAME)
	xelatex $(DOCNAME)

include astron-texmf/vcs-meta.make
